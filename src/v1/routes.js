const Express = require('express')

exports.initialize = () => {
  const v1 = Express.Router()

  v1.get('/ping', (req, res) => {
    res.json({ message: 'pong from v1!' })
    return
  })

  return v1
}
