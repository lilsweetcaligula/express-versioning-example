const Express = require('express')
const { deduceApiVersion } = require('./middleware/deduce_api_version.js')
const { handleAppError } = require('./middleware/handle_app_error.js')

exports.initialize = () => {
  const app = Express()

  app.use(deduceApiVersion(app))
  app.use(handleAppError())

  return app
}
