const Express = require('express')

exports.initialize = () => {
  const v2 = Express.Router()

  v2.get('/ping', (req, res) => {
    res.json({ message: 'pong from v2!' })
    return
  })

  return v2
}
