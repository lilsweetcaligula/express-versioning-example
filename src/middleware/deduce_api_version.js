const assert = require('assert-plus')
const v1 = require('../v1/routes.js')
const v2 = require('../v2/routes.js')
const { UnsupportedApiError } = require('../lib/errors.js')

const requestedVersion = req => {
  assert.object(req, 'req')
  assert.func(req.header, 'req.header')

  const accept_header = req.header('accept')

  if (typeof accept_header === 'string') {
    const match = accept_header
      .match(/^application\/vnd\.my_service\.v(\d{1,16})/)

    if (match) {
      return Number(match[1])
    }
  }

  return null
}

exports.deduceApiVersion = router => (req, res, next) => {
  const useApi = api => {
    assert.object(api, 'api')
    assert.func(api.initialize, 'api.initialize')

    router.use(api.initialize())
    next()

    return
  }

  const useNewestApi = () => useApi(v2)
  const isVersionSpecified = version => typeof version === 'number'

  const version = requestedVersion(req)

  if (isVersionSpecified(version)) {
    switch (version) {
      case 1: {
        useApi(v1)
        return
      }

      case 2: {
        useApi(v2)
        return
      }
      
      default:
        throw new UnsupportedApiError()
    }
  } else {
    useNewestApi()
  }

  return
}

