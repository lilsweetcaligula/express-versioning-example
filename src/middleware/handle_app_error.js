const { UnsupportedApiError } = require('../lib/errors.js')

exports.handleAppError = () => (err, req, res, next) => {
  if (!res.headersSent) {
    if (err instanceof UnsupportedApiError) {
      res.sendStatus(303)
      return
    }
  }
  
  next(err)

  return
}
