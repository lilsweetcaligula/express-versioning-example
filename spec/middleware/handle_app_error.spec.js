const Express = require('express')
const supertest = require('supertest')
const { handleAppError } = require('../../src/middleware/handle_app_error.js')
const { UnsupportedApiError } = require('../../src/lib/errors.js')

describe('error handling middleware', () => {
  const middlewareInstance = handleAppError()

  describe('when headers have already been sent', () => {
    it('passes the error to the next middleware', () => {
      const some_error = new Error()
      const fake_req = { _fake: 'request' }
      const fake_res = { _fake: 'response', headersSent: true }
      const fake_next = jasmine.createSpy()

      middlewareInstance(some_error, fake_req, fake_res, fake_next)

      expect(fake_next).toHaveBeenCalledWith(some_error)
    })
  })

  describe('when headers have not been sent', () => {
    describe('when the error is UnsupportedApiError', () => {
      let mock_app

      beforeEach(() => {
        mock_app = Express()
          .use((req, res) => { throw new UnsupportedApiError() })
          .use(handleAppError())
      })

      it('responds with status 303', async () => {
        await supertest(mock_app)
          .get('/')
          .expect(303)
          .expect(res => {
            expect(res.body).toEqual({})
          })
      })
    })

    describe('when other kind of error', () => {
      let mock_app

      beforeEach(() => {
        mock_app = Express()
          .use((req, res) => { throw new Error() })
          .use(handleAppError())
      })

      it('responds with status 500', async () => {
        await supertest(mock_app)
          .get('/')
          .expect(500)
          .expect(res => {
            expect(res.body).toEqual({})
          })
      })
    })
  })
})

