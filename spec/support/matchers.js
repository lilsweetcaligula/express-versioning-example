const assert = require('assert-plus')

/*
 * NOTE: Define your custom matchers here.
 *
 */

const NONE = {}

module.exports = jasmine => ({
  expectRejected: async (p, error_like = NONE) => {
    try {
      await p 
    } catch (err) {
      if (error_like !== NONE) {
        expect(err).toEqual(error_like)

        if (error_like && error_like.constructor) {
          assert.strictEqual(err.constructor, error_like.constructor)
        }
      }

      return
    }

    assert.fail('Expected an error to be thrown.')
  }
})

