const nock = require('nock')
const Jasmine = require('jasmine')

const jasmine_env = new Jasmine()

jasmine_env.loadConfigFile('./spec/support/jasmine.json')

jasmine_env.onComplete(have_all_tests_passed => {
  if (have_all_tests_passed) {
    return process.exit(0)
  } else {
    return process.exit(1)
  }
})

nock.disableNetConnect()

/* NOTE: Allow localhost connections in order to enable testing of local routes.
 */
nock.enableNetConnect('127.0.0.1')

jasmine_env.execute()
