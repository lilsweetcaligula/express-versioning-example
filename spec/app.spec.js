const supertest = require('supertest')
const App = require('../src/app.js')
const v1 = require('../src/v1/routes.js')
const v2 = require('../src/v2/routes.js')

describe('app', () => {
  describe('versioning', () => {
    const requestHeartbeat = () => supertest(App.initialize()).get('/ping')

    const requestHeartbeatVersion = v => requestHeartbeat()
      .set('accept', 'application/vnd.my_service.v' + v )

    describe('when v1 is requested', () => {
      beforeEach(() => {
        spyOn(v1, 'initialize').and.callThrough()
      })

      it('uses the v1 API', async () => {
        await requestHeartbeatVersion(1)
          .expect(200, { message: 'pong from v1!' })

        expect(v1.initialize).toHaveBeenCalled()
      })
    })

    describe('when v2 is requested', () => {
      beforeEach(() => {
        spyOn(v2, 'initialize').and.callThrough()
      })

      it('uses the v2 API', async () => {
        await requestHeartbeatVersion(2)
          .expect(200, { message: 'pong from v2!' })

        expect(v2.initialize).toHaveBeenCalled()
      })
    })

    describe('when unsupported version is requested', () => {
      it('responds with the See Other status', async () => {
        await requestHeartbeatVersion(9999)
          .expect(303)
      })
    })

    describe('when no version is specified', () => {
      beforeEach(() => {
        spyOn(v2, 'initialize').and.callThrough()
      })

      it('uses the newest version (v2)', async () => {
        await requestHeartbeat()
          .set('accept', 'application/json')
          .expect(200, { message: 'pong from v2!' })

        expect(v2.initialize).toHaveBeenCalled()
      })
    })

    describe('when non-numeric version is specified', () => {
      beforeEach(() => {
        spyOn(v2, 'initialize').and.callThrough()
      })

      it('uses the newest version (v2)', async () => {
        await requestHeartbeatVersion('abc')
          .expect(200, { message: 'pong from v2!' })

        expect(v2.initialize).toHaveBeenCalled()
      })
    })

    describe('when no "Accept" header is specified', () => {
      beforeEach(() => {
        spyOn(v2, 'initialize').and.callThrough()
      })

      it('uses the newest version (v2)', async () => {
        await requestHeartbeat()
          .expect(200, { message: 'pong from v2!' })

        expect(v2.initialize).toHaveBeenCalled()
      })
    })
  })
})

